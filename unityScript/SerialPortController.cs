using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.Text;
using System.Threading;
using UnityEngine.Events;

[Serializable]
public class RecvEvent : UnityEvent<int> {}

//发送数据定义
[Serializable]
public class SendData{
  public string deviceID; //设备ID或者设备名
  public int state;//设备数据
}

public class SerialPortController : MonoBehaviour
{
    [Header("串口名")] public string portName = "COM3";
    [Header("波特率")] public int baudRate = 9600;
    [Header("效验位")] public Parity parity = Parity.None;
    [Header("数据位")] public int dataBits = 8;
    [Header("停止位")] public StopBits stopBits = StopBits.One;

    private SerialPort sp = null;
    private Thread dataReceiveThread;
    private byte[] dataBytes;
    public RecvEvent recvDataEvent;
    private List<int> recvData = new List<int>();
    private void Start()
    {
        
        if (recvDataEvent == null) { 
            recvDataEvent = new RecvEvent(); 
        }
        OpenPortControl();
    }
    private void Update()
    {
        if (recvData.Count> 0) {
            recvDataEvent.Invoke(recvData[0]);
            recvData.RemoveRange(0, recvData.Count);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("键盘W键按下");
            SendDataToArduino("fan", 0);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            SendDataToArduino("fan", 1);
        }

    }
    /// <summary>
    /// 开启串口
    /// </summary>
    public void OpenPortControl()
    {
        sp = new SerialPort(portName, baudRate, parity, dataBits, stopBits);
        //串口初始化
        if (!sp.IsOpen)
        {
            sp.Open();
            sp.RtsEnable = true;
        }
        dataReceiveThread = new Thread(ReceiveData);//该线程用于接收串口数据 
        dataReceiveThread.Start();
    }
    /// <summary>
    /// 关闭串口
    /// </summary>
    public void ClosePortControl()
    {
        if (sp != null && sp.IsOpen)
        {
            sp.Close();//关闭串口
            sp.Dispose();//将串口从内存中释放掉
        }
    }

    private void ReceiveData()
    {
        int bytesToRead = 0;
        while (true)
        {
            if (sp != null && sp.IsOpen)
            {
                try
                {
                    dataBytes = new byte[1];
                    bytesToRead = sp.Read(dataBytes, 0, dataBytes.Length);
                    Thread.Sleep(10);
                    if (bytesToRead == 0)
                    {
                        continue;
                    }
                    else
                    {
                        //Debug.Log("Recv0:::"+ dataBytes[0]);
                        recvData.Add((int)dataBytes[0]);
                    }
                }
                catch (Exception e)
                {
                    Debug.Log(e.Message);
                }
            }
           
        }
    }

    void OnApplicationQuit()
    {
        ClosePortControl();
    }
    #region 发送数据
    public void WriteData(byte[] data,int offset,int count)
    {
        if (sp.IsOpen)
        {
           sp.Write(data, offset, count);
        }
    }
    #endregion
    public void WriteData(string text)
    {
        if (sp.IsOpen)
        {
            sp.Write(text);
        }
    }
    public void SendDataToArduino(string deviceID, int state)
    {
        SendData sendData = new SendData();
        sendData.deviceID = deviceID;
        sendData.state = state;
        string jsonTata = JsonUtility.ToJson(sendData);
        WriteData(jsonTata);
        Debug.Log("jsonData::::"+ jsonTata);

    }
}
