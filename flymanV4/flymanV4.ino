#include <ArduinoJson.h>
#include <MsTimer2.h>

// 电缸脉冲和方向引脚定义
#define PULSEPIN 5
#define DIRPIN 6

//微调按钮
#define UPPIN 8
#define DOWNPIN 9

//风扇引脚
#define fanPin 10

//2个磁电保护开关引脚
#define safeUpPin 12
#define safeDownPin 13

//最快脉冲发送间隔
#define DELAYTIMES 40
//发送脉冲数
#define NUMPULSE 40000

unsigned long delayTimes = 40;
//加速度
float acce = 0.2;

unsigned long times = 0;

//接收JSON数据
int State = 0;
boolean beginFlag = 0;
String inputString = "";      // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

//风扇和电缸状态
int motorDownState = 1;
int motorUpState = 0;

int safeupState = 0;
int safedownState = 0;

int resetFlag = 0;
//分配空间
StaticJsonDocument<200> doc;

//电缸运行状态
int upState = 0;
int downState = 0;

boolean upFlag = 0;
boolean downFlag = 0;

boolean startFlag = 0;

void flash() {
  if (startFlag == 1) {
    delayTimes = (unsigned long)(5000 / (125 - acce * times)); //减速时间间隔
    times++;
    if (times >= 200) {  //总减速时间
      startFlag = 0;
      times = 0;
    }
  }
}

//脉冲发送函数
void PulseNumbers(unsigned long);
//电缸上升
void motorUp(unsigned long);
//电杠下降
void motorDown(unsigned long);

//初始化
void setup() {
  // put your setup code here, to run once:
  MsTimer2::set(10, flash);  // 1000ms period
  MsTimer2::start();
  //引脚初始化
  pinMode(PULSEPIN, OUTPUT);
  pinMode(DIRPIN, OUTPUT);
  pinMode(fanPin, OUTPUT);
  digitalWrite(PULSEPIN, LOW);
  digitalWrite(DIRPIN, LOW);
  digitalWrite(fanPin, LOW);
  //串口初始化
  Serial.begin(115200);
}

//主函数
void loop() {
  //*************接收数据解析及处理**************/
  if (stringComplete) {
    stringComplete = false;
    DeserializationError error = deserializeJson(doc, inputString.c_str());
    if (error) {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.f_str());
      inputString = "";
      return;
    }
    String deviceID = doc["deviceID"];
    if (deviceID == "motor") {  //1 上升 0 下降
      int motorState = doc["state"];
      if (motorState == 1) {
        if (motorDownState == 1) {
          Serial.println("motorUp");
          motorUp(NUMPULSE);
          motorDownState =0;
          motorUpState = 1;
        }
      } else if (motorState == 0) {
        if (motorUpState == 1) {
          Serial.println("motorDown");
          motorDown(NUMPULSE);
          motorDownState = 1;
          motorUpState = 0;
        }
      }
    } else if (deviceID == "fan") {
      int fanState = doc["state"];  //1：开启 0： 关闭
      if (fanState == 1) {
        digitalWrite(fanPin, HIGH);
      } else if (fanState == 0) {
        digitalWrite(fanPin, LOW);
      }
    } else if (deviceID == "RESET") {
      int resetState = doc["state"];  //1:复位 0：不动作
      if (resetState == 1) {
        resetFlag = 1;
      }
    }
    inputString = "";  //    // clear the string:
  }
  //按钮检测
  if (digitalRead(UPPIN) == HIGH) {
    delay(10);
    if (digitalRead(UPPIN) == HIGH && upState == 0) {
      upState = 1;
      upFlag = 1;
    }
  } else if (digitalRead(UPPIN) == LOW && upState == 1) {
    upState = 0;
  }
  if (digitalRead(DOWNPIN) == HIGH) {
    delay(5);
    if (digitalRead(DOWNPIN) == HIGH && downState == 0) {
      downState = 1;
      downFlag = 1;
    }
  } else if (digitalRead(DOWNPIN) == LOW && downState == 1) {
    downState = 0;
  }
  //安全保护开关
  if (digitalRead(safeUpPin) == LOW) {
    delay(5);
    if (digitalRead(safeUpPin) == LOW && safeupState == 0) {
      safeupState = 1;
      //Serial.println("weixian1");
    }
  } else if (digitalRead(safeUpPin) == HIGH && safeupState == 1) {
    safeupState = 0;
  }
  if (digitalRead(safeDownPin) == LOW) {
    delay(5);
    if (digitalRead(safeDownPin) == LOW && safedownState == 0) {
      safedownState = 1;
      //Serial.println("weixian2");
    }
  } else if (digitalRead(safeDownPin) == HIGH && safedownState == 1) {
    safedownState = 0;
  }
  if (upFlag == 1) {
    upFlag = 0;
    //Serial.println("UP");
    motorUp(500);
  }
  if (downFlag == 1) {
    downFlag = 0;
    //Serial.println("DOWN");
    motorDown(500);
  }
  if (resetFlag == 1) {
    resetFlag = 0;
    if (motorUpState == 1) {
      motorDown(NUMPULSE);
      motorUpState =0;
      motorDownState =1;
    } else {
      Serial.write(0);
    }
  }
}

//脉冲发送函数
void PulseNumbers(unsigned long times) {
  digitalWrite(PULSEPIN, LOW);
  times = 0;
  delayTimes = DELAYTIMES;
  startFlag = 1;
  for (unsigned long index = 0; index < times; index++) {
    digitalWrite(PULSEPIN, HIGH);
    delayMicroseconds(delayTimes);
    digitalWrite(PULSEPIN, LOW);
    delayMicroseconds(delayTimes);
  }
  startFlag = 0;
}

void PulseNumbersTest(unsigned long times) {
  digitalWrite(PULSEPIN, LOW);
  for (unsigned long index = 0; index < times; index++) {
    digitalWrite(PULSEPIN, HIGH);
    delayMicroseconds(delayTimes);
    digitalWrite(PULSEPIN, LOW);
    delayMicroseconds(delayTimes);
  }
}
// 电缸上升
void motorUp(unsigned long times) {
  digitalWrite(DIRPIN, HIGH);
  PulseNumbers(times);
  Serial.write(1);
}
// 电缸下降
void motorDown(unsigned long times) {
  digitalWrite(DIRPIN, LOW);
  PulseNumbers(times);
  Serial.write(0);
}
//JSON数据接收
void serialEvent() {
  while (Serial.available()) {
    char inChar = Serial.read();
    if (inChar == '{' && beginFlag == 0) {
      beginFlag = 1;
    }
    if (beginFlag == 1) {
      if (inChar == '{') {
        State += 1;
      } else if (inChar == '}') {
        State -= 1;
      }
      inputString += inChar;
      if (State == 0) {
        beginFlag = 0;
        stringComplete = true;
      }
    }
  }
}
